# devopscamp - adidas OPS challenge developing

### Challenge
Your Mission, should you choose to accept it, is to code a web application.

#### Specifications
- web application shall recognize if a user is already logged in to social media platforms 
- and use this information to propose prefered login mechanism 
- any SDK can be used

#### Optional
- Configs to be stored in this gitlab project
- Application recognizes if user was logged in at adidas via social platform before
- integration via JS


